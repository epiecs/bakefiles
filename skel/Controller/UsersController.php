<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 */
class UsersController extends AppController 
{


    var $name = "Users";

    function startupProcess()
    {
	$this->singname = __("user");
	$this->plurname = __("users");	
		
	$this->set("singname", $this->User->singName);
	$this->set("plurname", $this->User->plurName);
	
	$this->set("displayfield", $this->User->displayField);
		
	//prep array for generating breadcrumbs

	$this->breadcrumbs = array(
	    ucfirst(__($this->plurname)) => "index"
	);
	
	parent::startupProcess();
    }

    function beforefilter()
    {		
	parent::beforeFilter();
	
	$this->Auth->allow('login', 'logout');
	$this->set("title_for_layout", $this->plurname);

	$this->set("Group_singname", $this->User->Group->singName);
	$this->set("Group_plurname", $this->User->Group->plurName);    
    }

/**
     * index method
     *
     * @return void
     */
     
    public function index() 
    {        
	$this->User->recursive = 0;
	$this->set('users', $this->paginate());
        
	$this->breadcrumbs[__('List %s',  $this->plurname)] = 'index';
	$this->set("title_for_layout", __('List %s',  $this->plurname));
	$this->set('breadcrumbs', $this->breadcrumbs);
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
     
    public function view($id = null) 
    {
	$this->User->id = $id;
	
	if (!$this->User->exists()) 
	{
	    throw new NotFoundException(__('Invalid %s',  $this->singname));
	}
	
	$this->set('user', $this->User->read(null, $id));
	
	$this->breadcrumbs[__('View %s',  $this->singname)] = 'view' . DS . $id;
	$this->set("title_for_layout", __('View %s',  $this->singname));
	$this->set('breadcrumbs', $this->breadcrumbs);
    }

        /**
    * add method
    *
    * @return void
    */
     
    public function add() 
    {
	if ($this->request->is('post')) 
	{
	    $this->User->create();
	    
	    if ($this->User->save($this->request->data)) 
	    {
		$this->Session->setFlash(__('The %s has been saved', $this->singname), 'flash' . DS .'success');
		$this->redirect(array('action' => 'index'));
	
	    }
	    else 
	    {
	        $this->Session->setFlash(__('The %s could not be saved. Please, try again.', $this->singname), 'flash' . DS .'error');
	    }
	}
	
	$groups = $this->User->Group->find('list');

	$this->set(compact('groups'));
	
	$this->breadcrumbs[__('Add %s',  $this->singname)] = 'add';
	$this->set("title_for_layout", __('Add %s',  $this->singname));
	$this->set('breadcrumbs', $this->breadcrumbs);
    }


    /**
    * edit method
    *
    * @throws NotFoundException
    * @param string $id
    * @return void
    */
    
    public function edit($id = null) 
    {
	$this->User->id = $id;
	
	if (!$this->User->exists()) 
	{
	    throw new NotFoundException(__('Invalid %s', $this->singname));
	}
	
	if ($this->request->is('post') || $this->request->is('put')) 
	{
	    if ($this->User->save($this->request->data)) 
	    {
		$this->Session->setFlash(__('The %s has been saved', $this->singname), 'flash' . DS .'success');
		$this->redirect(array('action' => 'index'));
	    } 
	    else 
	    {
		$this->Session->setFlash(__('The %s could not be saved. Please, try again.', $this->singname), 'flash' . DS .'error');
	    }
	} 
	else 
	{
	    $this->request->data = $this->User->read(null, $id);
	}
	
	$groups = $this->User->Group->find('list');

	$this->set(compact('groups'));
	
	$this->breadcrumbs[__('Edit %s',  $this->singname)] = 'edit' . DS . $id;
	$this->set("title_for_layout", __('Edit %s',  $this->singname));
	$this->set('breadcrumbs', $this->breadcrumbs);
    }

    /**
    * delete method
    *
    * @throws MethodNotAllowedException
    * @throws NotFoundException
    * @param string $id
    * @return void
    */
    
    public function delete($id = null) 
    {	
	$this->User->id = $id;
	
	if (!$this->User->exists()) 
	{
	    throw new NotFoundException(__('Invalid %s', $this->singname));
	}
	
	if ($this->User->delete()) 
	{
	    $this->Session->setFlash(__('The %s was successfully deleted', $this->singname), 'flash' . DS .'success');
	    $this->redirect(array('action' => 'index'));
	}
	
	$this->Session->setFlash(__('The %s was not deleted', $this->singname), 'flash' . DS .'error');
	$this->redirect(array('action' => 'index'));
    }
    
    public function login()
    {
	$this->layout = 'login';
	
	$this->set('applicationName', Configure::read('Application.name'));
        
	if ($this->request->is('post'))
	{
	    if ($this->Auth->login())
	    {
		$this->redirect($this->Auth->redirect());
	    }
	    else
	    {
		$this->Session->setFlash(__('Username or password is incorrect'), 'flash' . DS . 'error', array(), 'auth');
	    }
	}
        else
        {
            //basic en digest auth
            
            if ($this->Auth->login()) 
            {
                return $this->redirect($this->Auth->redirect());
            }
            else 
            {
                $this->Session->setFlash(__('Username or password is incorrect'), 'default', array(), 'auth');
            }
        }
    }

    public function logout()
    {
	$this->Session->setFlash(__('You were logged out'), 'flash' . DS . 'success');
	$this->redirect($this->Auth->logout());
    }

    //permission manager function
    
    function permissions()
    {
	$acorootnode = 'controllers';
	
	$this->loadModel('Aco');

        //remove beforefilter because it is not set from the view
	$acos = $this->Aco->find('threaded', array
            (
            'conditions' => array(
                'NOT' => array(
                    'alias' => array(
                        'beforefilter'
                    )
                )
            )
        ));

		
	$groups = $this->User->Group->find('all');

	$this->set('groups', $groups);
	$this->set('acos', $acos);
	
	if (!empty($this->request->data))
	{	    
	    $user = $this->Auth->user();

	    foreach ($groups as $group)
	    {
		$groupnames[] = $group['Group']['name'];
	    }

	    foreach ($acos[0]['children'] as $aco)
	    {
		/*
		foreach ($groupnames as $groupname)
		{
		    
		    if ($this->request->data['Rights'][$groupname][$aco['Aco']['alias']] == 1)
		    {
			
			$this->Acl->allow($groupname, $acorootnode . '/' . $aco['Aco']['alias'], '*');
		    }
		    else
		    {
			$this->Acl->deny($groupname, $acorootnode . '/' . $aco['Aco']['alias'], '*');
		    }
		}
		*/
		
		$aconame = $aco['Aco']['alias'];

		foreach ($aco['children'] as $acochild)
		{
		    foreach ($groupnames as $groupname)
		    {		  
			
			if ($this->request->data['Rights'][$groupname][$aconame][$acochild['Aco']['alias']] == 1)
			{
			    $this->Acl->allow($groupname, $acorootnode . '/' . $aco['Aco']['alias'] . '/' . $acochild['Aco']['alias']);
			}
			else
			{
			    $this->Acl->deny($groupname, $acorootnode . '/' . $aco['Aco']['alias'] . '/' . $acochild['Aco']['alias']);
			}
		    }
		}
	    }
	    
	    $this->Session->setFlash(__('The permissions were saved successfully'), 'flash' . DS . 'success');
	}

	if (empty($this->request->data))
	{
	    //debug($this->Acl->check('Administrators', 'controllers/Users'));
	    	    
	    foreach ($groups as $group)
	    {
		$groupnames[] = $group['Group']['name'];
	    }
	    	    
	    foreach ($acos[0]['children'] as $aco)
	    {	
		
		
		foreach ($groupnames as $groupname)
		{
		    if ($this->Acl->check($groupname, $acorootnode . '/' . $aco['Aco']['alias']))
		    {
			$this->request->data['Rights'][$groupname][$aco['Aco']['alias']] = array();
		    }
		    else
		    {
			$this->request->data['Rights'][$groupname][$aco['Aco']['alias']] = array();
		    }
		}
		
		$aconame = $aco['Aco']['alias'];

		foreach ($aco['children'] as $acochild)
		{
		    foreach ($groupnames as $groupname)
		    {
			if ($this->Acl->check($groupname, $acorootnode . '/' . $aco['Aco']['alias'] . '/' . $acochild['Aco']['alias']))
			{
			    $this->request->data['Rights'][$groupname][$aconame][$acochild['Aco']['alias']] = 1;
			}
			else
			{
			    $this->request->data['Rights'][$groupname][$aconame][$acochild['Aco']['alias']] = 0;
			}
		    }
		}
		
	    }
	
	}
    
	$this->breadcrumbs[__('Permissions')] = 'permissions';
	$this->set("title_for_layout", __('Permissions'));
	$this->set('breadcrumbs', $this->breadcrumbs);
    }

}
