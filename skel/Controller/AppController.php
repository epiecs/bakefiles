<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller 
{
    //set the theme to bootstrap
    //public $theme = 'Bootstrap';
    
    //initialize components + auth & acl
    public $components = array(
	'RequestHandler', 
	'Acl', 
	'Auth' => array
	(
            'authorize' => array
	    (
		'Actions' => array('actionPath' => 'controllers')
	    )
        ), 
	'Session'
    );
    
    //basic helper for generating bootstrap ready form elements
    
    public $helpers = array
    (
	'Bootstrap' => array(
	    'className' => 'TwitterBootstrap'
	),
	'BootstrapPaginator' => array(
	    'className' => 'TwitterBootstrapPaginator'
	),
	'Session',
        'Chart'
    );
    
    public function beforeFilter() 
    {
        $this->Auth->authenticate = array(
            'Form' => array(
                'userModel' => 'User'
            )
            /*
            ,
            'Digest' => array(
                'userModel' => 'User',
                'fields' => array(
                    'username' => 'username',
                    'password' => 'digest_password' 
                )
            )*/
        );
        
        $this->Auth->loginAction = array('controller' => 'users', 'action' => 'login');
        $this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'login');
	
	$this->Auth->authError = __('You do not have permission to access that location');
	
	$this->Auth->flash = array(
	    'element' => 'flash' . DS . 'error',
	    'key' => 'auth',
	    'params' => array()
	); 
	
	
	
	//login redirect, change to application path
        $this->Auth->loginRedirect = array('controller' => 'users', 'action' => 'index');
    }
    
}
