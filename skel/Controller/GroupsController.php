<?php
App::uses('AppController', 'Controller');
/**
 * Groups Controller
 *
 * @property Group $Group
 */
class GroupsController extends AppController 
{


    var $name = "Groups";

    function startupProcess()
    {
	$this->singname = __("group");
	$this->plurname = __("groups");	
		
	$this->set("singname", $this->Group->singName);
	$this->set("plurname", $this->Group->plurName);
	
	$this->set("displayfield", $this->Group->displayField);
		
	//prep array for generating breadcrumbs

	$this->breadcrumbs = array(
	    ucfirst(__($this->plurname)) => "index"
	);
	
	parent::startupProcess();
    }

    function beforefilter()
    {		
	parent::beforeFilter();
	
	//$this->Auth->allow();
	$this->set("title_for_layout", $this->plurname);

	$this->set("User_singname", $this->Group->User->singName);
	$this->set("User_plurname", $this->Group->User->plurName);    
    }

/**
     * index method
     *
     * @return void
     */
     
    public function index() 
    {        
	$this->Group->recursive = 0;
	$this->set('groups', $this->paginate());
        
	$this->breadcrumbs[__('List %s',  $this->plurname)] = 'index';
	$this->set("title_for_layout", __('List %s',  $this->plurname));
	$this->set('breadcrumbs', $this->breadcrumbs);
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
     
    public function view($id = null) 
    {
	$this->Group->id = $id;
	
	if (!$this->Group->exists()) 
	{
	    throw new NotFoundException(__('Invalid %s',  $this->singname));
	}
	
	$this->set('group', $this->Group->read(null, $id));
	
	$this->breadcrumbs[__('View %s',  $this->singname)] = 'view' . DS . $id;
	$this->set("title_for_layout", __('View %s',  $this->singname));
	$this->set('breadcrumbs', $this->breadcrumbs);
    }

        /**
    * add method
    *
    * @return void
    */
     
    public function add() 
    {
	if ($this->request->is('post')) 
	{
	    $this->Group->create();
	    
	    if ($this->Group->save($this->request->data)) 
	    {
		$this->Session->setFlash(__('The %s has been saved', $this->singname), 'flash' . DS .'success');
		$this->redirect(array('action' => 'index'));
	
	    }
	    else 
	    {
	        $this->Session->setFlash(__('The %s could not be saved. Please, try again.', $this->singname), 'flash' . DS .'error');
	    }
	}
	
	
	$this->breadcrumbs[__('Add %s',  $this->singname)] = 'add';
	$this->set("title_for_layout", __('Add %s',  $this->singname));
	$this->set('breadcrumbs', $this->breadcrumbs);
    }


    /**
    * edit method
    *
    * @throws NotFoundException
    * @param string $id
    * @return void
    */
    
    public function edit($id = null) 
    {
	$this->Group->id = $id;
	
	if (!$this->Group->exists()) 
	{
	    throw new NotFoundException(__('Invalid %s', $this->singname));
	}
	
	if ($this->request->is('post') || $this->request->is('put')) 
	{
	    if ($this->Group->save($this->request->data)) 
	    {
		$this->Session->setFlash(__('The %s has been saved', $this->singname), 'flash' . DS .'success');
		$this->redirect(array('action' => 'index'));
	    } 
	    else 
	    {
		$this->Session->setFlash(__('The %s could not be saved. Please, try again.', $this->singname), 'flash' . DS .'error');
	    }
	} 
	else 
	{
	    $this->request->data = $this->Group->read(null, $id);
	}
	
	
	$this->breadcrumbs[__('Edit %s',  $this->singname)] = 'edit' . DS . $id;
	$this->set("title_for_layout", __('Edit %s',  $this->singname));
	$this->set('breadcrumbs', $this->breadcrumbs);
    }

    /**
    * delete method
    *
    * @throws MethodNotAllowedException
    * @throws NotFoundException
    * @param string $id
    * @return void
    */
    
    public function delete($id = null) 
    {	
	$this->Group->id = $id;
	
	if (!$this->Group->exists()) 
	{
	    throw new NotFoundException(__('Invalid %s', $this->singname));
	}
	
	if ($this->Group->delete()) 
	{
	    $this->Session->setFlash(__('The %s was successfully deleted', $this->singname), 'flash' . DS .'success');
	    $this->redirect(array('action' => 'index'));
	}
	
	$this->Session->setFlash(__('The %s was not deleted', $this->singname), 'flash' . DS .'error');
	$this->redirect(array('action' => 'index'));
    }
}
