<?php
    //this file is called by all error templates

    $pluginDot = empty($plugin) ? null : $plugin . '.';
    
?>
<div class="span3"></div>
<div class="span6">
    <div class="well hero-unit">
        <h1>
            <?php echo __('So sorry :\'('); ?>
        </h1>
        <p>
            <?php echo __('But something went wrong!') . ' '; ?>
            <?php echo __('We are dealing with a: '); ?>
            <strong>
                <?php echo $errortitle ?>
            </strong>
        </p>
        <p>
        </p>
    </div>
    <h3>
        I've gathered some more information for you!
    </h3>
    
    <table class="table table-bordered table-striped">
        <tr>
            <td>
                <strong>
                    <?php echo __('Short description')?>                
                </strong>
            </td>
            <td>
                <?php echo $short_description; ?>
            </td>
        </tr>
        
        <?php
        if(isset($long_description))
        {
        ?>
            <tr>
                <td>
                    <strong>
                        <?php echo __('Long(er) description')?>                
                    </strong>
                </td>
                <td>
                     <?php echo $long_description; ?>
                </td>
            </tr>
        <?php
        }
        ?>
            
        <?php
        if(isset($phpcode))
        {
        ?>
            <tr>
            <tr>
                <td>
                    <strong>
                        <?php echo __('Code')?>                
                    </strong>
                </td>
                <td>
                    <code>
                        <?php echo $phpcode; ?>
                    </code>
                </td>
            </tr>
        <?php
        }
        ?>
            
        <?php
            if (Configure::read('debug') > 0 )
            {
        ?>
            <tr>
                <td>
                    <strong>
                        <?php echo __('Stack trace')?>                
                    </strong>
                </td>
                <td>
                    <?php echo $this->element('exception_stack_trace'); ?>
                </td>
            </tr>
         <?php } ?>
    </table>
    
</div>
<div class="span3"></div>