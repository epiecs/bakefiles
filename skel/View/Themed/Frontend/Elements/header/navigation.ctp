<div class="nav-collapse">
    <ul class="nav">
	
	<!-- Users -->
	<li class="active dropdown">
	    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		<?php echo __('Users'); ?> <b class="caret"></b>
	    </a>
	    <ul class="dropdown-menu">
		<li>
			<?php echo $this->Html->link(__('List all %s', 'users'), array('controller' => 'users'));?>
		</li>
		<li>
			<?php echo $this->Html->link(__('Add a new %s', 'user'), array('controller' => 'users', 'action' => 'add'));?>
		</li>
		    <li class="divider"></li>
		    <li class="nav-header">
			    <?php echo __('Management'); ?>
		    </li>
		
		<li>
		    <?php echo $this->Html->link(__('Permissions'), array('controller' => 'users', 'action' => 'permissions'));?>
		</li>
	    </ul>
	</li>
	
	<!-- Groups -->
	<li class="dropdown">
	    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		<?php echo __('Groups'); ?> <b class="caret"></b>
	     </a>
	    <ul class="dropdown-menu">
		<li>
		    <?php echo $this->Html->link(__('List all %s', 'groups'), array('controller' => 'groups'));?>
		</li>
		<li>
		    <?php echo $this->Html->link(__('Add a new', 'group'), array('controller' => 'groups', 'action' => 'add'));?>		
		</li>
	    </ul>
	</li>
	
    </ul>
</div>
<!-- /.nav-collapse -->