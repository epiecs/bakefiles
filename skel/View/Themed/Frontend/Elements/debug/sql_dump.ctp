<?php
/**
 * SQL Dump element.  Dumps out SQL log information
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Elements
 * @since         CakePHP(tm) v 1.3
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

//maximum query duration in ms
$maximum_query_duration = 100;
$warningthreshhold = 50;
$dangerthreshhold = 80;

if (!class_exists('ConnectionManager') || Configure::read('debug') < 2) {
	return false;
}

?>
<div class="row-fluid">
<div class="span1">
</div>
<div class="span10">
<?php
$noLogs = !isset($logs);
if ($noLogs):
	$sources = ConnectionManager::sourceList();

	$logs = array();
	foreach ($sources as $source):
		$db = ConnectionManager::getDataSource($source);
		if (!method_exists($db, 'getLog')):
			continue;
		endif;
		$logs[$source] = $db->getLog();
	endforeach;
endif;

if ($noLogs || isset($_forced_from_dbo_)):
	foreach ($logs as $source => $logInfo):
		$text = $logInfo['count'] > 1 ? 'queries' : 'query';
		printf(
			'<table class="table table-striped table-bordered" id="cakeSqlLog_%s" summary="Cake SQL Log" cellspacing="0">',
			preg_replace('/[^A-Za-z0-9_]/', '_', uniqid(time(), true))
		);
		printf('<caption>(%s) %s %s took %s ms</caption>', $source, $logInfo['count'], $text, $logInfo['time']);
	?>
	<thead>
		<tr><th>Nr</th><th>Query</th><th>Error</th><th>Affected</th><th>Num. rows</th><th>Took (ms)</th></tr>
	</thead>
	<tbody>
	<?php
		foreach ($logInfo['log'] as $k => $i) :
			$i += array('error' => '');
			if (!empty($i['params']) && is_array($i['params'])) {
				$bindParam = $bindType = null;
				if (preg_match('/.+ :.+/', $i['query'])) {
					$bindType = true;
				}
				foreach ($i['params'] as $bindKey => $bindVal) {
					if ($bindType === true) {
						$bindParam .= h($bindKey) ." => " . h($bindVal) . ", ";
					} else {
						$bindParam .= h($bindVal) . ", ";
					}
				}
				$i['query'] .= " , params[ " . rtrim($bindParam, ', ') . " ]";
			}
			echo "<tr>";
                            echo "<td>" . ($k + 1) . "</td>";
                            echo "<td>" . h($i['query']) . "</td>";
                            echo "<td>";
                            
                                if($i['error'] > 0)
                                {
                                    echo "<span class=\"badge badge-important\">" . $i['error'] . "</span>";
                                }
                            echo "</td>";    
                      
                            echo "
                                <td style = \"text-align: right\">{$i['affected']}</td>
                                <td style = \"text-align: right\">{$i['numRows']}</td>";
                            
                            //calculate tookprogress in percentage
                            $tookpercent = ($i['took'] / $maximum_query_duration) * 100;                        
                            echo "<td>";
                                                        
                            $additionalclass = "success";
                                                        
                            if($tookpercent > $warningthreshhold)
                            {
                                $additionalclass = "warning";
                            }

                            if($tookpercent > $dangerthreshhold)
                            {
                                $additionalclass = "danger";
                            }
                            
                            echo "<div class=\"progress progress-" . $additionalclass . "\">";
                                                                
                            echo"<div class=\"bar\"";
                            
                                echo "style=\"width:" . $tookpercent . "%;\"></div>
                            </div>";
                                
                            echo $i['took'] . 'ms';
                            
                            echo "</td>";
                               
                            echo "</tr>\n";
		endforeach;
	?>
	</tbody></table>
	<?php
	endforeach;
else:
	echo '<p>Encountered unexpected $logs cannot generate SQL log</p>';
endif;
?>
</div>
<div class="span1"></div>
</div>