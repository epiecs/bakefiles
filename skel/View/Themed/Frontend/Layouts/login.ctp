<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->script('jquery-1.8.0.min.js');
		echo $this->Html->script('bootstrap.min.js');
		echo $this->Html->script('application.js');

		echo $this->Html->css('bootstrap.min');
		
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>

        <style>
            input 
            {
                max-width: 100%;
            } 
        </style>
</head>
<body>   
	    
    <div class="container-fluid">
	<div class="row-fluid">

	    <?php echo $this->fetch('content'); ?>
	    
	</div>
	
	<?php echo $this->element('footer' . DS . 'footer'); ?>
    </div>
    
</body>
</html>