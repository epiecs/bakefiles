<?php
    $pluginDot = empty($plugin) ? null : $plugin . '.';
   
    echo $this->element('errors' . DS .  'error_template',
        array(
            'errortitle' => __('Scaffolding error'),
            'short_description' => __d('cake_dev', 'Method _scaffoldError in was not found in the controller'),
            'long_description' =>  __d('cake_dev', 'If you want to customize this error message, create %s', APP_DIR . DS . 'View' . DS . 'Errors' . DS . 'scaffold_error.ctp'),
            'phpcode' => "&lt;?php
                function _scaffoldError() {<br />

                }"
        )
    );
?>
