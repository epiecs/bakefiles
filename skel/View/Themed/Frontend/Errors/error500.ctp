<?php
    echo $this->element('errors' . DS .  'error_template',
        array(
            'errortitle' => __('Internal error'),
            'short_description' => __('An internal error has occurred'),
            'long_description' => $error
        )
    );
?>