<?php
   
    echo $this->element('errors' . DS .  'error_template',
        array(
            'errortitle' => __('Fatal error'),
            'short_description' => h($error->getMessage()),
            'long_description' =>  h($error->getFile()),
            'phpcode' => h($error->getLine())
        )
    );
?>