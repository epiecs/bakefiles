<?php
   
    echo $this->element('errors' . DS .  'error_template',
        array(
            'errortitle' => __('Missing plugin'),
            'short_description' => __d('cake_dev', 'The application is trying to load a file from the %s plugin', '<em>' . $plugin . '</em>'),
            'long_description' =>  __d('cake_dev', 'Make sure your plugin %s is in the ' . APP_DIR . DS . 'Plugin directory and was loaded', $plugin) . '<br>' .  __d('cake_dev', 'If you wish to load all plugins at once, use the following line in your ' . APP_DIR . DS . 'Config' . DS . 'bootstrap.php file'),
            'phpcode' => "CakePlugin::loadAll();"
        )
    );
?>