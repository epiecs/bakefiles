<?php
    $pluginDot = empty($plugin) ? null : $plugin . '.';
    
    echo $this->element('errors' . DS .  'error_template',
        array(
            'errortitle' => __('Missing datasource'),
            'short_description' => __d('cake_dev', 'Datasource class %s could not be found.', '<em>' . $pluginDot . $class . '</em>'),
            'long_description' => __d('cake_dev', 'If you want to customize this error message, create %s', APP_DIR . DS . 'View' . DS . 'Errors' . DS . 'missing_datasource.ctp')
        )
    );
?>