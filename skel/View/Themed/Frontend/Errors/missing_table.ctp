<?php
   
    echo $this->element('errors' . DS .  'error_template',
        array(
            'errortitle' => __('Missing database table'),
            'short_description' => __d('cake_dev', 'Table %1$s for model %2$s was not found in datasource %3$s.', '<em>' . $table . '</em>',  '<em>' . $class . '</em>', '<em>' . $ds . '</em>'),
            'long_description' =>  __d('cake_dev', 'If you want to customize this error message, create %s', APP_DIR . DS . 'View' . DS . 'Errors' . DS . 'missing_table.ctp')
        )
    );
?>