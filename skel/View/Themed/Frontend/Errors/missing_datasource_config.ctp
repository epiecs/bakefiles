<?php
   
    echo $this->element('errors' . DS .  'error_template',
        array(
            'errortitle' => __('Missing datasource configuration'),
            'short_description' => __d('cake_dev', 'The datasource configuration %1$s was not found in database.php.', '<em>' . $config . '</em>'),
            'long_description' => __d('cake_dev', 'If you want to customize this error message, create %s', APP_DIR . DS . 'View' . DS . 'Errors' . DS . 'missing_datasource_config.ctp')
        )
    );
?>