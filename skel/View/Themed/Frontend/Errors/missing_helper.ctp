<?php
    $pluginDot = empty($plugin) ? null : $plugin . '.';
   
    echo $this->element('errors' . DS .  'error_template',
        array(
            'errortitle' => __('Missing helper'),
            'short_description' => __d('cake_dev', '%s could not be found.', '<em>' . $pluginDot . $class . '</em>'),
            'long_description' => __d('cake_dev', 'Create the class %s below in file: %s', '<em>' . $class . '</em>', (empty($plugin) ? APP_DIR . DS : CakePlugin::path($plugin)) . 'View' . DS . 'Helper' . DS . $class . '.php'),
            'phpcode' => "&lt;?php
                class " . $class . " extends AppHelper {

                }"
        )
    );
?>