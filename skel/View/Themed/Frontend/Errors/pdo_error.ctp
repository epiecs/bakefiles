<?php
   
    echo $this->element('errors' . DS .  'error_template',
        array(
            'errortitle' => __('Pdo error'),
            'short_description' => __d('cake_dev', 'SQL Query') . '<br>' . $error->queryString,
            'long_description' =>  __d('cake_dev', 'SQL Query Params') . '<br>' . $error->params
        )
    );
?>