<?php
    
    echo $this->element('errors' . DS .  'error_template',
        array(
            'errortitle' => __('Missing action'),
            'short_description' => __d('cake_dev', 'The action %1$s is not defined in controller %2$s', '<em>' . $action . '</em>', '<em>' . $controller . '</em>'),
            'long_description' => __d('cake_dev', 'Create %1$s%2$s in file: %3$s.', '<em>' . $controller . '::</em>', '<em>' . $action . '()</em>', APP_DIR . DS . 'Controller' . DS . $controller . '.php'),
            'phpcode' => "&lt;?php
                class " . $controller . " extends AppController {\n

                <strong>
                        public function " .  $action . "() {

                        }
                </strong>
                }"
        )
    );
?>