<div class="span3">
    <div class="well">
	<ul class="nav nav-list">
	    <li class="nav-header">
		<?php echo __($plurname); ?>
	    </li>
	    <li>
		    <?php echo $this->Html->link('<i class="icon-plus"></i>' .  __('Add a new %s', $singname), array('controller' => 'groups', 'action' => 'add'), array('escape' => false)); ?>
	    </li>
	    
	    <!-- Model associations-->

	    <!-- User associations-->
	    <li class="nav-header">
		Users
	    </li>
	    <li>
		<?php echo $this->Html->link('<i class="icon-list"></i>' .  __('List all %s', $User_plurname), array('controller' => 'users', 'action' => 'index'), array('escape' => false)); ?> 
	    </li>
	    <li>
		<?php echo $this->Html->link('<i class="icon-plus"></i>' .  __('Add a new %s', $User_singname), array('controller' => 'users', 'action' => 'add'), array('escape' => false)); ?>
	    </li>
	</ul>
    </div>
</div>

<div class="span9">
    <?php echo $this->element('blocks' . DS . 'breadcrumbs'); ?>
    <?php echo $this->Session->flash(); ?>

    
    <div class="page-header">
	<h1>
	    <?php echo __(ucfirst($plurname)); ?>
	</h1>
    </div>
    
    <!-- Table containing model data -->
    
    <table class="table table-bordered table-striped">
	
                
	<!-- Generate all table headers -->
	<thead>
	    
	    <tr>
                <th>
                    <?php echo $this->Paginator->sort('name'); ?>
                </th>
                        <th class="actions"><?php echo __('Actions'); ?></th>
	    </tr>
	</thead>
	
	<!-- Generate all table data -->
	<tbody>
	    
	<?php
	foreach ($groups as $group)
	{ 
	?>

	  <tr id="<?php echo 'js-row' . $group['Group']['id']; ?>">
		<td><?php echo h($group['Group']['name']); ?>&nbsp;</td>

		<!-- Row actions -->

		<td class="actions">
		    <?php echo $this->Html->link(
			'<i class="icon-file icon-white"></i>', 
			array(
			    'action' => 'view', $group['Group']['id']
			), 
			array(
			    'escape' => false, 
			    'class' => 'btn btn-primary btn-small',
			    'title' => __('View'),
			    'alt' => __('View'),
			    'rel' => 'tooltip'
			)); 
		    ?>
		    <?php echo $this->Html->link(
			'<i class="icon-pencil icon-white"></i>', 
			array(
			    'action' => 'edit', $group['Group']['id']
			), 
			array(
			    'escape' => false, 
			    'class' => 'btn btn-inverse btn-small',
			    'title' => __('Edit'),
			    'alt' => __('Edit'),
			    'rel' => 'tooltip'
			)); 
		    ?>
		    <?php echo $this->Html->link(
			'<i class="icon-trash icon-white"></i>', 
			array(
			    'action' => 'delete', $group['Group']['id']
			), 
			array(
			    'escape' => false, 
			    'class' => 'btn btn-danger btn-small js-delete',
			    'title' => __('Delete'),
			    'alt' => __('Delete'),
			    'data-title' => 'Warning',
			    'data-description' => __('Are you sure that you want to delete %s ?', '<strong>' . $group['Group'][$displayfield] . '</strong>'),
				'rel' => 'tooltip'
			)); 
		    ?>
		</td>
	</tr>
<?php } ?>
	
	</tbody>
    </table>
    
    <?php echo $this->element('blocks' . DS . 'pagination'); ?>

	<?php echo $this->Html->link(
	'<i class="icon-plus icon-white"></i> ' . __('Add a new %s', $singname), 
	array(
	    'action' => 'add'
	), 
	array(
	    'escape' => false, 
	    'class' => 'btn btn-primary',
	    'title' => __('Add'),
	    'alt' => __('Add'),
	)); 
    ?>
    
    <?php echo $this->element(
	'modals' . DS . 'delete',
	array(
	    'title' => __('Warning'),
	    'cancel' => __('Cancel'),
	    'ok' => __('Delete')
	)
	); ?>
</div>