<div class="span3">
    <div class="well">
	<ul class="nav nav-list">
	    <li class="nav-header">
		<?php echo __($plurname); ?>
	    </li>
	    <li>
		    <?php echo $this->Html->link('<i class="icon-list"></i>' .  __('List all %s', $plurname), array('controller' => 'groups', 'action' => 'index'), array('escape' => false)); ?>
	    </li>
	    <li>
		    <?php echo $this->Html->link('<i class="icon-plus"></i>' .  __('Add a new %s', $singname), array('controller' => 'groups', 'action' => 'add'), array('escape' => false)); ?>
	    </li>
	    
	    <!-- Model associations-->

	    <!-- User associations-->
	    <li class="nav-header">
		Users
	    </li>
	    <li>
		<?php echo $this->Html->link('<i class="icon-list"></i>' .  __('List all %s', $User_plurname), array('controller' => 'users', 'action' => 'index'), array('escape' => false)); ?> 
	    </li>
	    <li>
		<?php echo $this->Html->link('<i class="icon-plus"></i>' .  __('Add a new %s', $User_singname), array('controller' => 'users', 'action' => 'add'), array('escape' => false)); ?>
	    </li>
	</ul>
    </div>
</div>

<div class="span9">
    <?php echo $this->element('blocks' . DS . 'breadcrumbs'); ?>
    <?php echo $this->Session->flash(); ?>

    <?php echo $this->Session->flash('auth'); ?>

    <div class="page-header">
	<h1>
	<?php echo __('Add a %s', $singname); ?>
	</h1>
    </div>
    
<?php echo $this->Bootstrap->create('Group'); ?>

	<?php
		echo $this->Bootstrap->input('name');
	?>
<?php echo $this->Bootstrap->button(__('Save')); ?>
</div>