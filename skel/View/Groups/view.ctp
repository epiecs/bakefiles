<div class="span3">
    <div class="well">
	<ul class="nav nav-list">
	    <li class="nav-header">
		<?php echo __($plurname); ?>
	    </li>
	    <li>
		    <?php echo $this->Html->link(
			'<i class="icon-trash"></i>' .  __('Delete this %s', $singname), 
			array('controller' => 'groups', 'action' => 'delete', $group['Group']['id']), 
			array(
			    'escape' => false, 
			    'class' => 'js-delete',
			    'title' => __('Delete'),
			    'alt' => __('Delete'),
			    'data-title' => 'Warning',
			    'data-description' => __('Are you sure that you want to delete %s ?', '<strong>' . $group['Group'][$displayfield] . '</strong>'))); 
		    ?>
	    </li>
	    <li>
		    <?php echo $this->Html->link('<i class="icon-pencil"></i>' .  __('Edit this %s', $singname), array('controller' => 'groups', 'action' => 'edit', $group['Group']['id']), array('escape' => false)); ?>
	    </li>
	    <li>
		    <?php echo $this->Html->link('<i class="icon-list"></i>' .  __('List all %s', $plurname), array('controller' => 'groups', 'action' => 'index'), array('escape' => false)); ?>
	    </li>
	    <li>
		    <?php echo $this->Html->link('<i class="icon-plus"></i>' .  __('Add a new %s', $singname), array('controller' => 'groups', 'action' => 'add'), array('escape' => false)); ?>
	    </li>
	    
	    <!-- Model associations-->

	    <!-- User associations-->
	    <li class="nav-header">
		Users
	    </li>
	    <li>
		<?php echo $this->Html->link('<i class="icon-list"></i>' .  __('List all %s', $User_plurname), array('controller' => 'users', 'action' => 'index'), array('escape' => false)); ?> 
	    </li>
	    <li>
		<?php echo $this->Html->link('<i class="icon-plus"></i>' .  __('Add a new %s', $User_singname), array('controller' => 'users', 'action' => 'add'), array('escape' => false)); ?>
	    </li>
	</ul>
    </div>
</div>

<div class="span9">
    <?php echo $this->element('blocks' . DS . 'breadcrumbs'); ?>
    <?php echo $this->Session->flash(); ?>

    <?php echo $this->Session->flash('auth'); ?>

    
    <div class="page-header">
	<h1>
	    <?php echo $group['Group'][$displayfield]; ?>
	</h1>
    </div>

    <!-- USER details -->
    
    <dl class="dl-horizontal">
	<dt><?php echo __('Name'); ?></dt>
	<dd>
	    <?php echo h($group['Group']['name']); ?>
			&nbsp;
		</dd>
    </dl>
<!-- HASONE connection-->     
<!-- HASMANY/HABTM connections -->	
	<div class="page-header">
	    <h1>
	    <?php echo __('Related %s', $User_plurname); ?>	    </h1>
	</div>
    
	
	    <?php if (!empty($group['User']))
	    { 
	    ?>
	<table class="table table-bordered table-striped">
	    <thead>
		<tr>
		<th><?php echo __('Username'); ?></th>
		<th><?php echo __('Description'); ?></th>
		    <th class="actions"><?php echo __('Actions'); ?></th>
		</tr>
	    </thead>
	    <tbody>
		
			<?php
		    $i = 0;
		
		foreach ($group['User'] as $user)
		{ ?>
		<tr>
			<td><?php echo $user['username']; ?></td>
			<td><?php echo $user['description']; ?></td>

		<!-- Row actions -->

		<td class="actions">
			    <?php echo $this->Html->link(
				'<i class="icon-file icon-white"></i>', 
				array(
				    'controller' => 'users', 'action' => 'view', $user['id']
				), 
				array(
				    'escape' => false, 
				    'class' => 'btn btn-primary btn-small',
				    'title' => __('View'),
				    'alt' => __('View'),
				    'rel' => 'tooltip'
				)); 
			    ?>
			    <?php echo $this->Html->link(
				'<i class="icon-pencil icon-white"></i>', 
				array(
				    'controller' => 'users', 'action' => 'edit', $user['id']
				), 
				array(
				    'escape' => false, 
				    'class' => 'btn btn-inverse btn-small',
				    'title' => __('Edit'),
				    'alt' => __('Edit'),
				    'rel' => 'tooltip'
				)); 
			    ?>
			    <?php echo $this->Html->link(
				'<i class="icon-trash icon-white"></i>', 
				array(
				    'controller' => 'users', 'action' => 'delete', $user['id']
				), 
				array(
				    'escape' => false, 
				    'class' => 'btn btn-danger btn-small js-delete',
				    'title' => __('Delete'),
				    'alt' => __('Delete'),
				    'data-title' => 'Warning',
				    'data-description' => __('Are you sure that you want to delete this %s ?', $User_singname),
				    'rel' => 'tooltip'	
				)); 
			    ?>
		</td>
		</tr>
	<?php } ?>
		
	    </tbody>
	
	</table>
	<?php } ?>	<?php echo $this->Html->link(
	'<i class="icon-plus icon-white"></i> ' . __('Add a new %s', $User_singname), 
	array(
	    'controller' => 'users', 'action' => 'add'
	), 
	array(
	    'escape' => false, 
	    'class' => 'btn btn-primary',
	    'title' => __('Edit'),
	    'alt' => __('Edit'),
	)); 
    ?>
    
    
    
    <?php echo $this->element(
	'modals' . DS . 'delete',
	array(
	    'title' => __('Warning'),
	    'cancel' => __('Cancel'),
	    'ok' => __('Delete')
	)
	); ?>
</div>