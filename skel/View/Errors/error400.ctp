<?php
    
    echo $this->element('errors' . DS .  'error_template',
        array(
            'errortitle' => __('Unfound address'),
            'short_description' => __('The requested address %s was not found on this server.')
        )
    );
?>