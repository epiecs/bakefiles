<?php
    $pluginDot = empty($plugin) ? null : $plugin . '.';
    
    echo $this->element('errors' . DS .  'error_template',
        array(
            'errortitle' => __('Missing database'),
            'short_description' => __d('cake_dev', 'Scaffold requires a database connection'),
            'long_description' => __d('cake_dev', 'Confirm you have created the file: %s', APP_DIR . DS . 'Config' . DS . 'database.php')
        )
    );
?>