<?php
    $pluginDot = empty($plugin) ? null : $plugin . '.';
   
    echo $this->element('errors' . DS .  'error_template',
        array(
            'errortitle' => __('Missing layout'),
            'short_description' => __d('cake_dev', 'The layout file %s can not be found or does not exist.', '<em>' . $file . '</em>'),
            'long_description' => __d('cake_dev', 'Confirm you have created the file: %s', '<em>' . $file . '</em>')
        )
    );
?>