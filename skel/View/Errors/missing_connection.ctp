<?php
    $pluginDot = empty($plugin) ? null : $plugin . '.';
    
    echo $this->element('errors' . DS .  'error_template',
        array(
            'errortitle' => __('Missing connection'),
            'short_description' => __d('cake_dev', '%s requires a database connection', $class),
            'long_description' => __d('cake_dev', '%s driver is NOT enabled', $class)
        )
    );
?>