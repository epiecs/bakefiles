<?php
   
    echo $this->element('errors' . DS .  'error_template',
        array(
            'errortitle' => __('Private method being called in an action'),
            'short_description' => __d('cake_dev', '%s%s cannot be accessed directly.', '<em>' . $controller . '::</em>', '<em>' . $action . '()</em>'),
            'long_description' =>  __d('cake_dev', 'If you want to customize this error message, create %s', APP_DIR . DS . 'View' . DS . 'Errors' . DS . 'private_action.ctp')
        )
    );
?>
