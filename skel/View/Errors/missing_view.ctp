<?php
   
    echo $this->element('errors' . DS . 'error_template',
        array(
            'errortitle' => __('Missing view'),
            'short_description' => __d('cake_dev', 'The view for %1$s%2$s was not found.', '<em>' . Inflector::camelize($this->request->controller) . 'Controller::</em>', '<em>' . $this->request->action . '()</em>'),
            'long_description' =>  __d('cake_dev', 'Confirm you have created the file: %s', $file)
        )
    );
?>