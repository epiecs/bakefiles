<?php
    $pluginDot = empty($plugin) ? null : $plugin . '.';
    
    echo $this->element('errors' . DS .  'error_template',
        array(
            'errortitle' => __('Missing controller'),
            'short_description' => __d('cake_dev', '%s could not be found.', '<em>' . $pluginDot . $class . '</em>'),
            'long_description' => __d('cake_dev', 'Create the class %s below in file: %s', '<em>' . $class . '</em>', (empty($plugin) ? APP_DIR . DS : CakePlugin::path($plugin)) . 'Controller' . DS . $class . '.php'),
            'phpcode' => "&lt;?php
                class " . $class . " extends " . $plugin . "AppController {
                }"
        )
    );
?>