<?php
/**
 * Helper that leverages chart generation with the help of the google 
 * visualisation api
 *
 * Requires that echo $this->fetch('script'); is set in the head of the layout
 * 
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @example https://code.google.com/apis/ajax/playground/?type=visualization Here you can find an overview of all charts. Extend the helper where neccesary
 * 
 * @copyright     Copyright 2012, Aanbieders.be
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 2.0.0
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('AppHelper', 'View/Helper');

class ChartHelper extends AppHelper 
{
    //Uses html helper to inject js code
    public $helpers = array('Html');
    
    public $defaultsettings = array();
    
    /**
     * Set some default options
     * 
     * @param View $View
     * @param type $settings
     */
    
    public function __construct(View $View, $settings = array()) 
    {
        $this->defaultsettings = array(
            'title' => 'Results',
            'id' => 'chart_' . uniqid(),
            'legend' => array(
                'position' => 'bottom'
            ),
            'width' => '100%',
            'height' => '100%'
        );
        
        parent::__construct($View, $settings);
    }
    
    /**
     * Fired before the rendering of the view file.
     * 
     * @param type $viewFile
     */
    
    public function beforeRender($viewFile) 
    {
        //Set the global js file from google, it will be placed in the top of layouts in 
        //echo $this->fetch('script');
        
        $this->Html->script('https://www.google.com/jsapi', array('inline' => false));
                
        parent::beforeRender($viewFile);
    }
    
    /**
     * 
     * @example https://developers.google.com/chart/interactive/docs/gallery/piechart?hl=nl#Configuration_Options pie chart
     * @example https://google-developers.appspot.com/chart/interactive/docs/gallery/barchart#Configuration_Options bar chart
     * @example https://google-developers.appspot.com/chart/interactive/docs/gallery/geochart#Configuration_Options geo chart
     * 
     * @param type $type
     * @param type $data
     * @param type $chartoptions
     * @return string Generated chart
     */
    
    public function draw($type = null, $data, $chartoptions = null)
    {
        if(!isset($chartoptions['id']))
        {
            $chartoptions['id'] = 'chart_' . uniqid();
        }
        
        $options = $this->setOptions($this->defaultsettings, $chartoptions);
        
        switch ($type) 
        {
            case 'pie':
                /*
                    Piecharts use 2 columns
                
                    $data = array(
                        array('Type', 'Status'),
                        array('Mapped', $report['Mappingcount']['mapped']),
                        array('Unmapped', $report['Mappingcount']['unmapped'])
                    );
                 
                    $options = array(            
                        'slices' => array(
                            array('color' => '#468847'),
                            array('color' => '#B94A48')
                        )
                    );
                */
                
                return $this->generateChart($data, 'PieChart', 'corechart', $options);
                break;
            case 'bar':
                
                /*
                    Bar charts use one row
                
                    $data = array(
                        array('Type', 'Mapped', 'Unmapped'),
                        array('Mapped', $report['Mappingcount']['mapped'], $report['Mappingcount']['unmapped'])
                    );
                 
                    $options = array(            
                        'colors' => array(
                            '#468847',
                            '#B94A48'
                        ),
                    );
                */
                
                return $this->generateChart($data, 'BarChart', 'corechart', $options);
                break;
            case 'gauge':
                
                /*
                    Gauges are based on a row basis, for every new row there is a new gauge generated
                    
                    $data = array(
                        array('Category', 'Ammount'),
                        array('Mapped', $report['Mappingcount']['mapped']),
                        //array('Unmapped', $report['Mappingcount']['unmapped'])
                    );
                
                    $options = array(            
                        'redFrom' => '90',
                        'redTo' => '100',
                        'yellowFrom' => '75',
                        'yellowTo' => '90',
                        'minorTicks' => '5'
                    );
                */
                
                return $this->generateChart($data, 'Gauge', 'gauge', $options);
                break;
            case 'line':
                return $this->generateChart($data, 'LineChart', 'corechart', $options);
                break;
            case 'geo':
                /*
                    Geo charts are a bit more complex. you can however divide them on a region/country basis

                    $data = array(
                        array('Postcode', 'Scrapes'),
                        array('3600', 5),
                        array('9000', 10),
                        array('3800', 12),
                        array('3500', 3)
                    );

                    $options = array(            
                        'title' => 'Scrapes per city',
                        'region' => 'BE',
                        'displayMode' => 'markers',
                        'markerOpacity' => 1.0
                    );
                */
                
                return $this->generateChart($data, 'GeoChart', 'geochart', $options);
                break;
            case 'combo':
                return $this->generateChart($data, 'ComboChart', 'corechart', $options);
                break;
            default:
                return 'The chart type (' . $type . ') is not supported (yet)';
                break;
        }
    }
    
    /**
     * Generates a dimensional chart based on delivered data and options
     * 
     * Expects data in the following format
     * $data = array(
     *     '<Column title>', <column title>,
     *     '<value>', <value>,
     *     '<value>', <value>,
     *     '<...>', <...>
     * );
     * 
     * @param type $data The chart data
     * @param type $options Options set here will override the default settings
     * @return string Generated chart
     */
    
    private function generateChart($data, $charttype = null, $package = null, $options = null)
    {  
        $chart = "
        <script type='text/javascript'>

            // Load the Visualization API and the piechart package.
            google.load('visualization', '1', {'packages':['" . $package . "']});

            // Set a callback to run when the Google Visualization API is loaded.
            google.setOnLoadCallback(drawChart);

            // Callback that creates and populates a data table,
            // instantiates the pie chart, passes in the data and
            // draws it.
            function drawChart() {

            // Create the data table.
            var data = new google.visualization.arrayToDataTable(" . json_encode($data) . ");
        ";        
        
        // Set chart options
        $chart .= 'var options = ' . json_encode($options) . ';';

        // Instantiate and draw our chart, passing in some options.
        $chart .= 'var chart = new google.visualization.' . $charttype . '(document.getElementById("' . $options['id'] . '"));';
        $chart .= 'chart.draw(data, options);';
        
        
        $chart .= '}</script>';

        $chart .= '<div id="' . $options['id'] . '"></div>';
        
        return $chart;
    }
    
    /**
     * Overwrites the default options with the given options
     * 
     * @param type $defaultoptions
     * @param type $setoptions
     * @return array Options
     */
    
    private function setOptions($defaultoptions, $setoptions)
    {
        $options = $defaultoptions;
        
        if(is_array($options))
        {
            foreach ($setoptions as $key => $value)
            {
                if(is_array($setoptions[$key]) && isset($defaultoptions[$key]))
                { 
                    $options[$key] = $this->setOptions($defaultoptions[$key], $setoptions[$key]);
                }
                else
                {
                    $options[$key] = $value;                
                }
                
            }
 
            return $options;
        }
    }
}