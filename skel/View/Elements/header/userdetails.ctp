<div class="pull-right">
    <ul class="nav">
	<?php
	    $currentuser = $this->Session->read('Auth.User');
	?>
	<!-- Profile & logout -->
	<li class="dropdown">
	    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		<?php echo __('Welcome, %s', $currentuser['username']); ?> <b class="caret"></b>
	    </a>
	    <ul class="dropdown-menu">
		<li>
			<?php echo $this->Html->link(__('My profile'), array('controller' => 'users', 'action' => 'edit', $currentuser['id']));?>
		</li>

		    <li class="divider"></li>
		<li>
		    <?php echo $this->Html->link(__('Logout'), array('controller' => 'users', 'action' => 'logout'));?>
		</li>
	    </ul>
	</li>
	
    </ul>
</div>