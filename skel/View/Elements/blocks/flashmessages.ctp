<?php

    echo $this->Bootstrap->flash('success', array('closable' => true));
    echo $this->Bootstrap->flash('error', array('closable' => true));
    echo $this->Bootstrap->flash('info', array('closable' => true));
    echo $this->Bootstrap->flash('warning', array('closable' => true));

    echo $this->Bootstrap->flash('auth', array('closable' => true));
    
?>