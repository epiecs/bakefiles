<div class="modal hide fade" id="delete-modal">
    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">×</button>
	<h3>
	    <?php echo title; ?>
	</h3>
    </div>
    <div class="modal-body">
	<p class="modalmessage"></p>
    </div>
    <div class="modal-footer">
	<a href="#" class="btn" data-dismiss="modal">
	    <?php echo $cancel; ?>
	</a>
	<a href="#" class="btn btn-primary js-delete-confirmed">
	    <?php echo $ok; ?>
	</a>
    </div>
</div>