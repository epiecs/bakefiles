<div class="span5"></div>

<div class="span2">

<div class="page-header">
    <h1><?php echo $applicationName; ?></h1>
</div>    
    
<?php    
    echo $this->Bootstrap->flash('auth', array('closable' => true));
?>
    
<?php echo $this->Bootstrap->create('User', array('class' => 'well')); ?>
	<?php
		echo $this->Bootstrap->input('username');
		echo $this->Bootstrap->input('password');
	?>
<?php echo $this->Bootstrap->button(__('Login'), array('class' => 'btn-primary')); ?>
    
</div>

<div class="span5"></div>
