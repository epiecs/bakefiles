<div class="span3">
    <div class="well">
	<ul class="nav nav-list">
	    <li class="nav-header">
		<?php echo __($plurname); ?>
	    </li>
	    <li>
		    <?php echo $this->Html->link(
			'<i class="icon-trash"></i>' .  __('Delete this %s', $singname), 
			array('controller' => 'users', 'action' => 'delete', $user['User']['id']), 
			array(
			    'escape' => false, 
			    'class' => 'js-delete',
			    'title' => __('Delete'),
			    'alt' => __('Delete'),
			    'data-title' => 'Warning',
			    'data-description' => __('Are you sure that you want to delete %s ?', '<strong>' . $user['User'][$displayfield] . '</strong>'))); 
		    ?>
	    </li>
	    <li>
		    <?php echo $this->Html->link('<i class="icon-pencil"></i>' .  __('Edit this %s', $singname), array('controller' => 'users', 'action' => 'edit', $user['User']['id']), array('escape' => false)); ?>
	    </li>
	    <li>
		    <?php echo $this->Html->link('<i class="icon-list"></i>' .  __('List all %s', $plurname), array('controller' => 'users', 'action' => 'index'), array('escape' => false)); ?>
	    </li>
	    <li>
		    <?php echo $this->Html->link('<i class="icon-plus"></i>' .  __('Add a new %s', $singname), array('controller' => 'users', 'action' => 'add'), array('escape' => false)); ?>
	    </li>
	    
	    <!-- Model associations-->

	    <!-- Group associations-->
	    <li class="nav-header">
		Groups
	    </li>
	    <li>
		<?php echo $this->Html->link('<i class="icon-list"></i>' .  __('List all %s', $Group_plurname), array('controller' => 'groups', 'action' => 'index'), array('escape' => false)); ?> 
	    </li>
	    <li>
		<?php echo $this->Html->link('<i class="icon-plus"></i>' .  __('Add a new %s', $Group_singname), array('controller' => 'groups', 'action' => 'add'), array('escape' => false)); ?>
	    </li>
	</ul>
    </div>
</div>

<div class="span9">
    <?php echo $this->element('blocks' . DS . 'breadcrumbs'); ?>
    <?php echo $this->Session->flash(); ?>

    <?php echo $this->Session->flash('auth'); ?>

    
    <div class="page-header">
	<h1>
	    <?php echo $user['User'][$displayfield]; ?>
	</h1>
    </div>

    <!-- USER details -->
    
    <dl class="dl-horizontal">
	<dt><?php echo __('Username'); ?></dt>
	<dd>
	    <?php echo h($user['User']['username']); ?>
			&nbsp;
		</dd>
	<dt><?php echo __('Password'); ?></dt>
	<dd>
	    <?php echo h($user['User']['password']); ?>
			&nbsp;
		</dd>
	<dt><?php echo __('Digest Password'); ?></dt>
	<dd>
	    <?php echo h($user['User']['digest_password']); ?>
			&nbsp;
		</dd>
	<dt><?php echo __('Description'); ?></dt>
	<dd>
	    <?php echo h($user['User']['description']); ?>
			&nbsp;
		</dd>
	<dt><?php echo __('Group'); ?></dt>
	<dd>
	    <?php echo $this->Html->link($user['Group']['name'], array('controller' => 'groups', 'action' => 'view', $user['Group']['id'])); ?>
			&nbsp;
		</dd>
    </dl>
<!-- HASONE connection-->     
<!-- HASMANY/HABTM connections -->    
    
    <?php echo $this->element(
	'modals' . DS . 'delete',
	array(
	    'title' => __('Warning'),
	    'cancel' => __('Cancel'),
	    'ok' => __('Delete')
	)
	); ?>
</div>