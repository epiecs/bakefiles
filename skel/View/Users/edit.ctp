<div class="span3">
    <div class="well">
	<ul class="nav nav-list">
	    <li class="nav-header">
		<?php echo __($plurname); ?>
	    </li>
	    <li>
		    <?php echo $this->Html->link('<i class="icon-list"></i>' .  __('List all %s', $plurname), array('controller' => 'users', 'action' => 'index'), array('escape' => false)); ?>
	    </li>
	    <li>
		    <?php echo $this->Html->link('<i class="icon-plus"></i>' .  __('Add a new %s', $singname), array('controller' => 'users', 'action' => 'add'), array('escape' => false)); ?>
	    </li>
	    
	    <!-- Model associations-->

	    <!-- Group associations-->
	    <li class="nav-header">
		Groups
	    </li>
	    <li>
		<?php echo $this->Html->link('<i class="icon-list"></i>' .  __('List all %s', $Group_plurname), array('controller' => 'groups', 'action' => 'index'), array('escape' => false)); ?> 
	    </li>
	    <li>
		<?php echo $this->Html->link('<i class="icon-plus"></i>' .  __('Add a new %s', $Group_singname), array('controller' => 'groups', 'action' => 'add'), array('escape' => false)); ?>
	    </li>
	</ul>
    </div>
</div>

<div class="span9">
    <?php echo $this->element('blocks' . DS . 'breadcrumbs'); ?>
    <?php echo $this->Session->flash(); ?>

    <?php echo $this->Session->flash('auth'); ?>

    <div class="page-header">
	<h1>
	<?php echo __('Edit %s',  $this->request->data['User'][$displayfield]); ?>
	</h1>
    </div>
    
<?php echo $this->Bootstrap->create('User'); ?>

	<?php
		echo $this->Form->hidden('id');
		echo $this->Bootstrap->input('username');
		echo $this->Bootstrap->input('password');
		echo $this->Bootstrap->input('digest_password');
		echo $this->Bootstrap->input('description');
		echo $this->Bootstrap->input('group_id');
	?>
<?php echo $this->Bootstrap->button(__('Save')); ?>
</div>