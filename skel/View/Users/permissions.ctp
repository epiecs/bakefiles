<div class="span3">
    <div class="well">
	<ul class="nav nav-list">
	    <li class="nav-header">
		<?php echo __($plurname); ?>
	    </li>
	    <li>
		    <?php echo $this->Html->link('<i class="icon-plus"></i>' .  __('Add a new %s', $singname), array('controller' => 'users', 'action' => 'add'), array('escape' => false)); ?>
	    </li>
	    <li>
		    <?php echo $this->Html->link('<i class="icon-plus"></i>' .  __('List all %s', $plurname), array('controller' => 'users', 'action' => 'index'), array('escape' => false)); ?>
	    </li>
	    
	    <!-- Model associations-->

	    <!-- Group associations-->
	    <li class="nav-header">
		Groups
	    </li>
	    <li>
		<?php echo $this->Html->link('<i class="icon-list"></i>' .  __('List all %s', $Group_plurname), array('controller' => 'groups', 'action' => 'index'), array('escape' => false)); ?> 
	    </li>
	    <li>
		<?php echo $this->Html->link('<i class="icon-plus"></i>' .  __('Add a new %s', $Group_singname), array('controller' => 'groups', 'action' => 'add'), array('escape' => false)); ?>
	    </li>

	</ul>
    </div>
</div>

<div class="span9">
    <?php echo $this->element('blocks' . DS . 'breadcrumbs'); ?>
    <?php echo $this->Session->flash(); ?>


    <div class="page-header">
	<h1>
	    <?php echo __('Permission management'); ?>
	</h1>
    </div>

    <?php echo $this->Bootstrap->create('User', array('action' => 'permissions')); ?>
    <?php
    echo '<table class="table table-bordered table-striped">';

    echo '<thead><tr>';
    echo '<th>' . __('Actions') . '</th>';

    foreach ($groups as $group)
    {
	echo '<th>' . $group['Group']['name'] . '</th>';
	$groupnames[] = $group['Group']['name'];
    }
    
    echo '</tr></thead><tbody>';

    foreach ($acos[0]['children'] as $aco)
    {
	echo '<tr>';
	echo '<td>' . $aco['Aco']['alias'] . '</td>';

	foreach ($groupnames as $groupname)
	{
	    echo '<td>';
	    //echo $this->Form->checkbox('Rights.' . $groupname . '.' . $aco['Aco']['alias'], array('type' => 'checkbox', 'label' => false));
	    echo'</td>';
	}

	echo '</tr>';

	foreach ($aco['children'] as $acochild)
	{
	    if($acochild['Aco']['alias'] != "beforefilter")
	    {
		echo '<tr>';
		echo '<td>&nbsp;&nbsp;&nbsp;' . $acochild['Aco']['alias'] . '</td>';

		foreach ($groupnames as $groupname)
		{

		    echo '<td>';
		    
		    echo $this->Form->checkbox('Rights.' . $groupname . '.' . $aco['Aco']['alias'] . '.' . $acochild['Aco']['alias'], array('type' => 'checkbox', 'label' => false));	    
		    
		    echo'</td>';
		}
		
		echo '</tr>';
	   
	    }
	}
    }


    echo '</tbody><tfoot><tr>';
    echo '<th>' . __('Actions') . '</th>';

    foreach ($groups as $group)
    {
	echo '<th>' . $group['Group']['name'] . '</th>';
	$groupnames[] = $group['Group']['name'];
    }
    echo '</tr></tfoot>';

    echo '</table>';
    ?>   
    <?php echo $this->Bootstrap->button(__('Save')); ?>
</div>