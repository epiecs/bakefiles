$().ready(function()
{
    //delete modal
    
    $('.js-delete').on('click', function(){
		
	//set description for modal
	$('#delete-modal .modal-body p').html($(this).attr('data-description'));
		
	//set button href
	$('#delete-modal .btn-primary').attr('href', ($(this).attr('href')));
		
	//set title for modal
	$('#delete-modal .modal-header h3').html($(this).attr('data-title'));
						
	$('#delete-modal').modal('show');
		
	return false;
    });
    
    $("a[rel='tooltip']").tooltip();
});