<?php
App::uses('AppModel', 'Model');

App::uses('AuthComponent', 'Controller/Component');
App::uses('DigestAuthenticate', 'Controller/Component/Auth');
/**
 * User Model
 *
 * @property Group $Group
 */
class User extends AppModel
{

    public $actsAs = array('Acl' => array('type' => 'requester'));


    /**
    * Display field
    *
    * @var string
    */

    public $displayField = 'username';


    /**
    * Validationdomain
    *
    * Used for translating model error messages
    *
    * @var string
    */

    public $validationDomain = 'validation_errors';

    //initialize public variables

    public $singName;
    public $plurName;

    //override the default cakephp constructor

    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);

	$this->singName = __('User');
        $this->plurName = __('Users');
    }

        /**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'username' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'password' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'digest_password' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'description' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'group_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

    //The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
            'Group' => array(
                    'className' => 'Group',
                    'foreignKey' => 'group_id',
                    'conditions' => '',
                    'fields' => '',
                    'order' => ''
            )
    );

    public function beforeSave($options = array())
    {
        $this->data['User']['digest_password'] = DigestAuthenticate::password(
            $this->data['User']['username'],
            $this->data['User']['password'],
            env('SERVER_NAME')
        );


        $this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
        return true;
    }

    //fix aro alias

    public function afterSave($created = false, $options = array())
    {
        $this->Aro->save(array('alias'=>$this->data[$this->alias]['username']));
    }

    //parentnode function for acl requests

    public function parentNode()
    {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        if (isset($this->data['User']['group_id'])) {
            $groupId = $this->data['User']['group_id'];
        } else {
            $groupId = $this->field('group_id');
        }
        if (!$groupId) {
            return null;
        } else {
            return array('Group' => array('id' => $groupId));
        }
    }

    //this function enables group based acl and disables user based acl -- way quicker

    public function bindNode($user)
    {
        return array('model' => 'Group', 'foreign_key' => $user['User']['group_id']);
    }
}
