<?php
/**
 * Controller bake template file
 *
 * Allows templating of Controllers generated from bake.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.classes
 * @since         CakePHP(tm) v 1.3
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

echo "<?php\n";
echo "App::uses('{$plugin}AppController', '{$pluginPath}Controller');\n";
?>
/**
 * <?php echo $controllerName; ?> Controller
 *
<?php
if (!$isScaffold) {
	$defaultModel = Inflector::singularize($controllerName);
	echo " * @property {$defaultModel} \${$defaultModel}\n";
	if (!empty($components)) {
		foreach ($components as $component) {
			echo " * @property {$component}Component \${$component}\n";
		}
	}
}
?>
 */
class <?php echo $controllerName; ?>Controller extends <?php echo $plugin; ?>AppController 
{

<?php 
    if ($isScaffold)
    {
?>
/**
 * Scaffold
 *
 * @var mixed
 */
	public $scaffold;

<?php
    }
    else
    { 
	$standardcomponents = array(
	);
		
	if (count($helpers))
	{
	    echo "/**\n * Helpers\n *\n * @var array\n */\n";
	    echo "\tpublic \$helpers = array(";
	    for ($i = 0, $len = count($helpers); $i < $len; $i++)
	    {
		if ($i != $len - 1)
		{
		    echo "'" . Inflector::camelize($helpers[$i]) . "', ";
		}
		else
		{
		    echo "'" . Inflector::camelize($helpers[$i]) . "'";
		}
		
	    }
	    
	    echo ");\n\n";
	}
	
	if (count($components))
	{
	    echo "/**\n * Components\n *\n * @var array\n */\n";
	    echo "\tpublic \$components = array(";
	    
	    for ($i = 0, $len = count($components); $i < $len; $i++)
	    {
		if ($i != $len - 1)
		{
		    echo "'" . Inflector::camelize($components[$i]) . "', ";
		}
		else
		{
		    echo "'" . Inflector::camelize($components[$i]) . "'";			
		}
	    }
	    
	    echo ");\n";
	}
	
echo '
    var $name = "' . $controllerName . '";

    function startupProcess()
    {
	$this->singname = __("' . strtolower(Inflector::singularize($controllerName)) . '");
	$this->plurname = __("' . strtolower($controllerName) . '");	
		
	$this->set("singname", $this->' . Inflector::singularize($controllerName) . '->singName);
	$this->set("plurname", $this->' . Inflector::singularize($controllerName) . '->plurName);
	
	$this->set("displayfield", $this->' . Inflector::singularize($controllerName) . '->displayField);
		
	//prep array for generating breadcrumbs

	$this->breadcrumbs = array(
	    ucfirst($this->plurname) => "index"
	);
	
	parent::startupProcess();
    }

    function beforefilter()
    {		
	parent::beforeFilter();
	
	//$this->Auth->allow();
	$this->set("title_for_layout", $this->plurname);
';

    foreach (array('belongsTo', 'hasAndBelongsToMany', 'hasMany', 'hasOne') as $assoc)
    {
        foreach ($modelObj->{$assoc} as $associationName => $relation)
        {
            if (!empty($associationName))
            {
                $otherModelName = $this->_modelName($associationName);
                
                echo "\n\t" . '$this->set("' . $otherModelName . '_singname", $this->' . Inflector::singularize($controllerName) . '->' . $otherModelName . '->singName);';
                echo "\n\t" . '$this->set("' . $otherModelName . '_plurname", $this->' . Inflector::singularize($controllerName) . '->' . $otherModelName . '->plurName);';
            }
        }
    }

echo '    
    }
';

echo "\n";

	
    echo trim($actions) . "\n";

    } ?>
}
