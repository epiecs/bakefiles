<div class="span3">
    <div class="well">
	<ul class="nav nav-list">
	    <li class="nav-header">
		<?php echo "<?php echo __(\$plurname); ?>\n"; ?>
	    </li>
	    <li>
		    <?php echo "<?php echo \$this->Html->link('<i class=\"icon-plus\"></i>' .  __('Add a new %s', \$singname), array('controller' => '" . $pluralVar . "', 'action' => 'add'), array('escape' => false)); ?>\n"; ?>
	    </li>

	    <!-- Model associations-->
<?php
$done = array();
foreach ($associations as $type => $data)
{
    foreach ($data as $alias => $details)
    {
	if ($details['controller'] != $this->name && !in_array($details['controller'], $done))
	{
echo "\n	    <!-- " . Inflector::humanize(Inflector::underscore($alias)) . " associations-->";

echo "\n	    <li class=\"nav-header\">
		" . Inflector::humanize($details['controller']) .  "
	    </li>\n";

echo "	    <li>
		<?php echo \$this->Html->link('<i class=\"icon-list\"></i>' .  __('List all %s', \$" . $alias . "_plurname), array('controller' => '{$details['controller']}', 'action' => 'index'), array('escape' => false)); ?>
	    </li>\n";

echo "	    <li>
		<?php echo \$this->Html->link('<i class=\"icon-plus\"></i>' .  __('Add a new %s', \$" . $alias . "_singname), array('controller' => '{$details['controller']}', 'action' => 'add'), array('escape' => false)); ?>
	    </li>\n";
	    $done[] = $details['controller'];
	}
    }
}
?>
	</ul>
    </div>
</div>

<div class="span9">
    <?php echo "<?php echo \$this->element('blocks' . DS . 'breadcrumbs'); ?>\n"; ?>
    <?php echo "<?php echo \$this->element('blocks' . DS . 'flashmessages'); ?>\n"; ?>

    <div class="page-header">
	<h1>
	    <?php echo "<?php echo __(ucfirst(\$plurname)); ?>\n"; ?>
	</h1>
    </div>

    <!-- Table containing model data -->

    <table class="table table-bordered table-striped">

        <?php
            $skippedfields = array('modified', 'created')
        ?>

	<!-- Generate all table headers -->
        <thead>
            <tr>
    <?php  foreach ($fields as $field){ ?>
    <?php if(!in_array($field, $skippedfields))
    {
        if($field == 'id')
        {
    ?>      <th>
                <i class="icon-trash"></i>
              </th>
    <?php
        }
        else
        {
    ?>
  <th>
        <?php echo "        <?php echo \$this->Paginator->sort('{$field}'); ?>\n"; ?>
              </th>
    <?php
        }
    }
    ?>
    <?php } ?>
<th class="actions"><?php echo "<?php echo __('Actions'); ?>"; ?></th>
	    </tr>
	</thead>

	<!-- Generate all table data -->
	<tbody>

<?php
	echo "	<?php
	foreach (\${$pluralVar} as \${$singularVar})
	{
	?>\n";
	echo "\n\t  <tr id=\"<?php echo 'js-row' . \${$singularVar}['{$modelClass}']['{$primaryKey}']; ?>\">\n";
	    foreach ($fields as $field)
	    {
                if(!in_array($field, $skippedfields))
                {
                    if($field == 'id')
                    {

                        echo "\t\t<td><?php echo \$this->Form->input('multidelete.' . \${$singularVar}['{$modelClass}']['{$field}'], array(
                            'value' => \${$singularVar}['{$modelClass}']['{$field}'],
                            'type' => 'checkbox',
                            'label' => false
                        ));?></td>\n";

                    }
                    else
                    {
		$isKey = false;

		if (!empty($associations['belongsTo']))
		{
		    foreach ($associations['belongsTo'] as $alias => $details)
		    {
			if ($field === $details['foreignKey'])
			{
			    $isKey = true;
			    echo "\n\t\t<td>
		    <?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>
		</td>\n\n";
			    break;
			}
		    }
		}

		if ($isKey !== true)
		{
		    echo "\t\t<td><?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>&nbsp;</td>\n";
		}
                    }
                }
	    }

	    echo "\n\t\t<!-- Row actions -->\n";

	    echo "\n\t\t<td class=\"actions\">\n";
echo "		    <?php echo \$this->Html->link(
			'<i class=\"icon-file icon-white\"></i>',
			array(
			    'action' => 'view', \${$singularVar}['{$modelClass}']['{$primaryKey}']
			),
			array(
			    'escape' => false,
			    'class' => 'btn btn-primary btn-small',
			    'title' => __('View'),
			    'alt' => __('View'),
			    'rel' => 'tooltip'
			));
		    ?>\n";
echo "		    <?php echo \$this->Html->link(
			'<i class=\"icon-pencil icon-white\"></i>',
			array(
			    'action' => 'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}']
			),
			array(
			    'escape' => false,
			    'class' => 'btn btn-inverse btn-small',
			    'title' => __('Edit'),
			    'alt' => __('Edit'),
			    'rel' => 'tooltip'
			));
		    ?>\n";
echo "		    <?php echo \$this->Html->link(
			'<i class=\"icon-trash icon-white\"></i>',
			array(
			    'action' => 'delete', \${$singularVar}['{$modelClass}']['{$primaryKey}']
			),
			array(
			    'escape' => false,
			    'class' => 'btn btn-danger js-delete btn-small',
			    'title' => __('Delete'),
			    'alt' => __('Delete'),
			    'data-title' => 'Warning',
			    'data-description' => __('Are you sure that you want to delete %s ?', '<strong>' . \${$singularVar}['{$modelClass}'][\$displayfield] . '</strong>'),
				'rel' => 'tooltip'
			));
		    ?>\n";

	    echo "\t\t</td>\n";
	echo "\t</tr>\n";

	echo "<?php } ?>\n";
?>
	</tbody>
    </table>

    <?php echo "<?php echo \$this->element('blocks' . DS . 'pagination'); ?>\n"?>

<?php

echo "	<?php echo \$this->Html->link(
	'<i class=\"icon-plus icon-white\"></i> ' . __('Add a new %s', \$singname),
	array(
	    'action' => 'add'
	),
	array(
	    'escape' => false,
	    'class' => 'btn btn-primary',
	    'title' => __('Add'),
	    'alt' => __('Add'),
	));
    ?>\n";
?>

    <?php
	echo "<?php echo \$this->element(
	'modals' . DS . 'delete',
	array(
	    'title' => __('Warning'),
	    'cancel' => __('Cancel'),
	    'ok' => __('Delete')
	)
	); ?>\n";
    ?>
</div>