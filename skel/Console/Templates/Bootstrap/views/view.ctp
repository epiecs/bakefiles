<div class="span3">
    <div class="well">
	<ul class="nav nav-list">
	    <li class="nav-header">
		<?php echo "<?php echo __(\$plurname); ?>\n"; ?>
	    </li>
	    <li>
		    <?php echo "<?php echo \$this->Html->link(
			'<i class=\"icon-trash\"></i>' .  __('Delete this %s', \$singname),
			array('controller' => '" . $pluralVar . "', 'action' => 'delete', \${$singularVar}['{$modelClass}']['{$primaryKey}']),
			array(
			    'escape' => false,
			    'class' => 'js-delete',
			    'title' => __('Delete'),
			    'alt' => __('Delete'),
			    'data-title' => 'Warning',
			    'data-description' => __('Are you sure that you want to delete %s ?', '<strong>' . \${$singularVar}['{$modelClass}'][\$displayfield] . '</strong>')));
		    ?>\n"; ?>
	    </li>
	    <li>
		    <?php echo "<?php echo \$this->Html->link('<i class=\"icon-pencil\"></i>' .  __('Edit this %s', \$singname), array('controller' => '" . $pluralVar . "', 'action' => 'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('escape' => false)); ?>\n"; ?>
	    </li>
	    <li>
		    <?php echo "<?php echo \$this->Html->link('<i class=\"icon-list\"></i>' .  __('List all %s', \$plurname), array('controller' => '" . $pluralVar . "', 'action' => 'index'), array('escape' => false)); ?>\n"; ?>
	    </li>
	    <li>
		    <?php echo "<?php echo \$this->Html->link('<i class=\"icon-plus\"></i>' .  __('Add a new %s', \$singname), array('controller' => '" . $pluralVar . "', 'action' => 'add'), array('escape' => false)); ?>\n"; ?>
	    </li>

	    <!-- Model associations-->
<?php
$done = array();
foreach ($associations as $type => $data)
{
    foreach ($data as $alias => $details)
    {
	if ($details['controller'] != $this->name && !in_array($details['controller'], $done))
	{
echo "\n	    <!-- " . Inflector::humanize(Inflector::underscore($alias)) . " associations-->";

echo "\n	    <li class=\"nav-header\">
		" . Inflector::humanize($details['controller']) .  "
	    </li>\n";

echo "	    <li>
		<?php echo \$this->Html->link('<i class=\"icon-list\"></i>' .  __('List all %s', \$" . $alias . "_plurname), array('controller' => '{$details['controller']}', 'action' => 'index'), array('escape' => false)); ?>
	    </li>\n";

echo "	    <li>
		<?php echo \$this->Html->link('<i class=\"icon-plus\"></i>' .  __('Add a new %s', \$" . $alias . "_singname), array('controller' => '{$details['controller']}', 'action' => 'add'), array('escape' => false)); ?>
	    </li>\n";
	    $done[] = $details['controller'];
	}
    }
}
?>
	</ul>
    </div>
</div>

<div class="span9">
    <?php echo "<?php echo \$this->element('blocks' . DS . 'breadcrumbs'); ?>\n"; ?>
    <?php echo "<?php echo \$this->element('blocks' . DS . 'flashmessages'); ?>\n"; ?>

    <div class="page-header">
	<h1>
	    <?php echo "<?php echo \${$singularVar}['{$modelClass}'][\$displayfield]; ?>\n"; ?>
	</h1>
    </div>
<?php
    $skippedfields = array('id', 'modified', 'created', $singularVar . '_id');

echo "\n    <!-- USER details -->\n";
?>
    <dl class="dl-horizontal">
<?php
    foreach ($fields as $field)
    {
	if(!in_array($field, $skippedfields))
	{
	    $isKey = false;

	    if (!empty($associations['belongsTo']))
	    {
		foreach ($associations['belongsTo'] as $alias => $details)
		{
		    if ($field === $details['foreignKey'])
		    {
			$isKey = true;
			echo "\t<dt><?php echo __('" . Inflector::humanize(Inflector::underscore($alias)) . "'); ?></dt>\n";
			echo "\t<dd>\n\t    <?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t\t&nbsp;\n\t\t</dd>\n";
			break;
		    }
		}
	    }

	    if ($isKey !== true)
	    {
		echo "\t<dt><?php echo __('" . Inflector::humanize($field) . "'); ?></dt>\n";
		echo "\t<dd>\n\t    <?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>\n\t\t\t&nbsp;\n\t\t</dd>\n";
	    }
	}
    }
?>
    </dl>
<?php
    echo "<!-- HASONE connection--> ";

    if (!empty($associations['hasOne']))
    {
	foreach ($associations['hasOne'] as $alias => $details)
	{
?>
	<div class="page-header">
	    <h1>
		<?php echo "<?php echo __('Related %s', \$" . $alias . "_singname); ?>"; ?>
	    </h1>
	</div>

	<?php echo "<?php
	    if (!empty(\${$singularVar}['{$alias}']))
	    {
	    ?>\n"; ?>
		<dl class="dl-horizontal">
<?php
	foreach ($details['fields'] as $field)
	{
	    if(!in_array($field, $skippedfields))
	    {
		echo "\t\t  <dt><?php echo __('" . Inflector::humanize($field) . "'); ?></dt>\n";
		echo "\t\t<dd>\n\t<?php echo \${$singularVar}['{$alias}']['{$field}']; ?>\n&nbsp;</dd>\n";
	    }
	}
?>
		</dl>
	<?php echo "<?php
	    }
	    ?>\n"; ?>

	<?php echo "
	    <?php echo \$this->Html->link(
	    '<i class=\"icon-white icon-pencil\"></i> ' . __('Edit this %s', \$" . $alias . "_singname),
		array(
		    'controller' => '{$details['controller']}', 'action' => 'edit', \${$singularVar}['{$alias}']['{$details['primaryKey']}']
		    ),
		array(
		    'escape' => false,
		    'class' => 'btn btn-primary btn-small',
		    'title' => __('View'),
		    'alt' => __('View')
		)); ?>\n"; ?>

<?php
	}
    }
?>

<?php

    echo "<!-- HASMANY/HABTM connections -->";

    if (empty($associations['hasMany']))
    {
	$associations['hasMany'] = array();
    }

    if (empty($associations['hasAndBelongsToMany']))
    {
	$associations['hasAndBelongsToMany'] = array();
    }

    $relations = array_merge($associations['hasMany'], $associations['hasAndBelongsToMany']);

    $i = 0;

    foreach ($relations as $alias => $details)
    {

	$otherSingularVar = Inflector::variable($alias);
	$otherPluralHumanName = Inflector::humanize($details['controller']);
?>
	<div class="page-header">
	    <h1>
	    <?php echo "<?php echo __('Related %s', \$" . $alias . "_plurname); ?>"; ?>
	    </h1>
	</div>

	<?php echo "
	    <?php if (!empty(\${$singularVar}['{$alias}']))
	    {
	    ?>\n";
	?>
	<table class="table table-bordered table-striped">
	    <thead>
		<tr>
<?php
		    foreach ($details['fields'] as $field)
		    {
			if(!in_array($field, $skippedfields))
			{
			    echo "\t\t<th><?php echo __('" . Inflector::humanize($field) . "'); ?></th>\n";
			}
		    }
?>
		    <th class="actions"><?php echo "<?php echo __('Actions'); ?>"; ?></th>
		</tr>
	    </thead>
	    <tbody>

		<?php
		echo "\t<?php
		    \$i = 0;

		foreach (\${$singularVar}['{$alias}'] as \${$otherSingularVar})
		{ ?>\n";
		echo "\t\t<tr>\n";
			foreach ($details['fields'] as $field)
			{
			    if(!in_array($field, $skippedfields))
			    {
				echo "\t\t\t<td><?php echo \${$otherSingularVar}['{$field}']; ?></td>\n";

			    }
			}

			echo "\n\t\t<!-- Row actions -->\n";

			echo "\n\t\t<td class=\"actions\">\n";

echo "			    <?php echo \$this->Html->link(
				'<i class=\"icon-file icon-white\"></i>',
				array(
				    'controller' => '{$details['controller']}', 'action' => 'view', \${$otherSingularVar}['{$details['primaryKey']}']
				),
				array(
				    'escape' => false,
				    'class' => 'btn btn-primary btn-small',
				    'title' => __('View'),
				    'alt' => __('View'),
				    'rel' => 'tooltip'
				));
			    ?>\n";

echo "			    <?php echo \$this->Html->link(
				'<i class=\"icon-pencil icon-white\"></i>',
				array(
				    'controller' => '{$details['controller']}', 'action' => 'edit', \${$otherSingularVar}['{$details['primaryKey']}']
				),
				array(
				    'escape' => false,
				    'class' => 'btn btn-inverse btn-small',
				    'title' => __('Edit'),
				    'alt' => __('Edit'),
				    'rel' => 'tooltip'
				));
			    ?>\n";

echo "			    <?php echo \$this->Html->link(
				'<i class=\"icon-trash icon-white\"></i>',
				array(
				    'controller' => '{$details['controller']}', 'action' => 'delete', \${$otherSingularVar}['{$details['primaryKey']}']
				),
				array(
				    'escape' => false,
				    'class' => 'btn btn-danger js-delete btn-small',
				    'title' => __('Delete'),
				    'alt' => __('Delete'),
				    'data-title' => 'Warning',
				    'data-description' => __('Are you sure that you want to delete this %s ?', \$" . $alias . "_singname),
				    'rel' => 'tooltip'
				));
			    ?>\n";

	    echo "\t\t</td>\n";

		echo "\t\t</tr>\n";

		echo "\t<?php } ?>\n";
?>

	    </tbody>

	</table>
	<?php echo "<?php } ?>"; ?>
<?php

echo "	<?php echo \$this->Html->link(
	'<i class=\"icon-plus icon-white\"></i> ' . __('Add a new %s', \$" . $alias . "_singname),
	array(
	    'controller' => '{$details['controller']}', 'action' => 'add'
	),
	array(
	    'escape' => false,
	    'class' => 'btn btn-primary',
	    'title' => __('Edit'),
	    'alt' => __('Edit'),
	));
    ?>\n";
?>

<?php
    }
?>


    <?php
	echo "<?php echo \$this->element(
	'modals' . DS . 'delete',
	array(
	    'title' => __('Warning'),
	    'cancel' => __('Cancel'),
	    'ok' => __('Delete')
	)
	); ?>\n";
    ?>
</div>