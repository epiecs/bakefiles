<div class="span3">
    <div class="well">
	<ul class="nav nav-list">
	    <li class="nav-header">
		<?php echo "<?php echo __(\$plurname); ?>\n"; ?>
	    </li>
	    <li>
		    <?php echo "<?php echo \$this->Html->link('<i class=\"icon-list\"></i>' .  __('List all %s', \$plurname), array('controller' => '" . $pluralVar . "', 'action' => 'index'), array('escape' => false)); ?>\n"; ?>
	    </li>
	    <li>
		    <?php echo "<?php echo \$this->Html->link('<i class=\"icon-plus\"></i>' .  __('Add a new %s', \$singname), array('controller' => '" . $pluralVar . "', 'action' => 'add'), array('escape' => false)); ?>\n"; ?>
	    </li>

	    <!-- Model associations-->
<?php
$done = array();
foreach ($associations as $type => $data)
{
    foreach ($data as $alias => $details)
    {
	if ($details['controller'] != $this->name && !in_array($details['controller'], $done))
	{
echo "\n	    <!-- " . Inflector::humanize(Inflector::underscore($alias)) . " associations-->";

echo "\n	    <li class=\"nav-header\">
		" . Inflector::humanize($details['controller']) .  "
	    </li>\n";

echo "	    <li>
		<?php echo \$this->Html->link('<i class=\"icon-list\"></i>' .  __('List all %s', \$" . $alias . "_plurname), array('controller' => '{$details['controller']}', 'action' => 'index'), array('escape' => false)); ?>
	    </li>\n";

echo "	    <li>
		<?php echo \$this->Html->link('<i class=\"icon-plus\"></i>' .  __('Add a new %s', \$" . $alias . "_singname), array('controller' => '{$details['controller']}', 'action' => 'add'), array('escape' => false)); ?>
	    </li>\n";
	    $done[] = $details['controller'];
	}
    }
}
?>
	</ul>
    </div>
</div>

<div class="span9">
    <?php echo "<?php echo \$this->element('blocks' . DS . 'breadcrumbs'); ?>\n"; ?>
    <?php echo "<?php echo \$this->element('blocks' . DS . 'flashmessages'); ?>\n"; ?>
    
    <div class="page-header">
	<h1>
	<?php
	    if(strpos($action, 'add') !== false)
	    {
		echo "<?php echo __('Add a %s', \$singname); ?>\n";
	    }
	    else
	    {
		echo "<?php echo __('Edit %s',  \$this->request->data['{$modelClass}'][\$displayfield]); ?>\n";
	    }
	?>
	</h1>
    </div>

<?php echo "<?php echo \$this->Bootstrap->create('{$modelClass}'); ?>\n"; ?>

<?php
	echo "\t<?php\n";
	foreach ($fields as $field)
	{
	    if (strpos($action, 'add') !== false && $field == $primaryKey)
	    {
		continue;
	    }
	    elseif (!in_array($field, array('created', 'modified', 'updated', 'id')))
	    {
		echo "\t\techo \$this->Bootstrap->input('{$field}');\n";
	    }
	    elseif (in_array($field, array('id')))
	    {
		echo "\t\techo \$this->Form->hidden('{$field}');\n";
	    }

	}

	if (!empty($associations['hasAndBelongsToMany']))
	{
	    foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData)
	    {
		echo "\t\techo \$this->Bootstrap->input('{$assocName}');\n";
	    }
	}
    echo "\t?>\n";

    echo "<?php echo \$this->Bootstrap->button(__('Save')); ?>\n";
?>
</div>