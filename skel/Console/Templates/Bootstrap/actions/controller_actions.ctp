<?php
/**
 * Bake Template for Controller action generation.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.actions
 * @since         CakePHP(tm) v 1.3
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>

	/**
     * <?php echo $admin ?>index method
     *
     * @return void
     */
     
    public function <?php echo $admin ?>index() 
    {        
	$this-><?php echo $currentModelName ?>->recursive = 0;
	$this->set('<?php echo $pluralName ?>', $this->paginate());
        
	$this->breadcrumbs[__('List %s',  $this->plurname)] = 'index';
	$this->set("title_for_layout", __('List %s',  $this->plurname));
	$this->set('breadcrumbs', $this->breadcrumbs);
    }

    /**
     * <?php echo $admin ?>view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
     
    public function <?php echo $admin ?>view($id = null) 
    {
	$this-><?php echo $currentModelName; ?>->id = $id;
	
	if (!$this-><?php echo $currentModelName; ?>->exists()) 
	{
	    throw new NotFoundException(__('Invalid %s',  $this->singname));
	}
	
	$this->set('<?php echo $singularName; ?>', $this-><?php echo $currentModelName; ?>->read(null, $id));
	
	$this->breadcrumbs[__('View %s',  $this->singname)] = 'view' . DS . $id;
	$this->set("title_for_layout", __('View %s',  $this->singname));
	$this->set('breadcrumbs', $this->breadcrumbs);
    }

    <?php $compact = array(); ?>
    /**
    * <?php echo $admin ?>add method
    *
    * @return void
    */
     
    public function <?php echo $admin ?>add() 
    {
	if ($this->request->is('post')) 
	{
	    $this-><?php echo $currentModelName; ?>->create();
	    
	    if ($this-><?php echo $currentModelName; ?>->save($this->request->data)) 
	    {
<?php if ($wannaUseSession){?>
		$this->Session->setFlash(__('The %s has been saved', $this->singname), 'default', array(), 'success');
		$this->redirect(array('action' => 'index'));
<?php }else{ ?>
		$this->flash(__('%s saved.', $this->singname), array('action' => 'index'), 'default', array(), 'success');
<?php } ?>	
	    }
	    else 
	    {
<?php if ($wannaUseSession){ ?>
	        $this->Session->setFlash(__('The %s could not be saved. Please, try again.', $this->singname), 'default', array(), 'error');
<?php } ?>
	    }
	}
	
<?php
    foreach (array('belongsTo', 'hasAndBelongsToMany') as $assoc)
    {
	foreach ($modelObj->{$assoc} as $associationName => $relation)
	{
	    if (!empty($associationName))
	    {
		$otherModelName = $this->_modelName($associationName);
		$otherPluralName = $this->_pluralName($associationName);
	echo "\t\${$otherPluralName} = \$this->{$currentModelName}->{$otherModelName}->find('list');\n";
		$compact[] = "'{$otherPluralName}'";
	    }
	}
    }
    
    if (!empty($compact))
    {
echo "\n\t\$this->set(compact(".join(', ', $compact)."));\n";
    }
?>
	
	$this->breadcrumbs[__('Add %s',  $this->singname)] = 'add';
	$this->set("title_for_layout", __('Add %s',  $this->singname));
	$this->set('breadcrumbs', $this->breadcrumbs);
    }

<?php $compact = array(); ?>

    /**
    * <?php echo $admin ?>edit method
    *
    * @throws NotFoundException
    * @param string $id
    * @return void
    */
    
    public function <?php echo $admin; ?>edit($id = null) 
    {
	$this-><?php echo $currentModelName; ?>->id = $id;
	
	if (!$this-><?php echo $currentModelName; ?>->exists()) 
	{
	    throw new NotFoundException(__('Invalid %s', $this->singname));
	}
	
	if ($this->request->is('post') || $this->request->is('put')) 
	{
	    if ($this-><?php echo $currentModelName; ?>->save($this->request->data)) 
	    {
<?php if ($wannaUseSession){ ?>
		$this->Session->setFlash(__('The %s has been saved', $this->singname), 'default', array(), 'success');
		$this->redirect(array('action' => 'index'));
<?php }else{ ?>
		$this->flash(__('The %s has been saved.', $this->singname), array('action' => 'index'), 'default', array(), 'success');
<?php } ?>
	    } 
	    else 
	    {
<?php if ($wannaUseSession){ ?>
		$this->Session->setFlash(__('The %s could not be saved. Please, try again.', $this->singname), 'default', array(), 'error');
<?php } ?>
	    }
	} 
	else 
	{
	    $this->request->data = $this-><?php echo $currentModelName; ?>->read(null, $id);
	}
	
<?php
	    foreach (array('belongsTo', 'hasAndBelongsToMany') as $assoc){
		foreach ($modelObj->{$assoc} as $associationName => $relation){
		    if (!empty($associationName))
		    {
			    $otherModelName = $this->_modelName($associationName);
			    $otherPluralName = $this->_pluralName($associationName);
			    echo "\t\${$otherPluralName} = \$this->{$currentModelName}->{$otherModelName}->find('list');\n";
			    $compact[] = "'{$otherPluralName}'";
		    }
		}
	    }
	     
	    if (!empty($compact)){
	echo "\n\t\$this->set(compact(".join(', ', $compact)."));\n";
	    }
?>
	
	$this->breadcrumbs[__('Edit %s',  $this->singname)] = 'edit' . DS . $id;
	$this->set("title_for_layout", __('Edit %s',  $this->singname));
	$this->set('breadcrumbs', $this->breadcrumbs);
    }

    /**
    * <?php echo $admin ?>delete method
    *
    * @throws MethodNotAllowedException
    * @throws NotFoundException
    * @param string $id
    * @return void
    */
    
    public function <?php echo $admin; ?>delete($id = null) 
    {	
        if ($this->request->is('post') && isset($this->request->data['multidelete'])) 
	{
	    $this-><?php echo $currentModelName; ?>->deleteAll(array('<?php echo $currentModelName; ?>.id' => $this->request->data['multidelete']));
	    $this->Session->setFlash(__('The %s were successfully deleted', $this->plurname), 'default', array(), 'success');
	    $this->redirect($this->referer());
        }
    
	$this-><?php echo $currentModelName; ?>->id = $id;
	
	if (!$this-><?php echo $currentModelName; ?>->exists()) 
	{
	    throw new NotFoundException(__('Invalid %s', $this->singname));
	}
	
	if ($this-><?php echo $currentModelName; ?>->delete()) 
	{
<?php if ($wannaUseSession){ ?>
	    $this->Session->setFlash(__('The %s was successfully deleted', $this->singname), 'default', array(), 'success');
	    $this->redirect(array('action' => 'index'));
<?php }else{ ?>
	    $this->flash(__('The %s was successfully deleted', $this->singname), array('action' => 'index'), 'default', array(), 'success');
<?php } ?>
	}
	
<?php if ($wannaUseSession){ ?>
	$this->Session->setFlash(__('The %s was not deleted', $this->singname), 'default', array(), 'error');
<?php }else{ ?>
	$this->flash(__('The %s was not deleted', $this->singname), array('action' => 'index'), 'default', array(), 'error');
<?php } ?>
	$this->redirect(array('action' => 'index'));
    }