<?php
/**
 * AppShell file
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         CakePHP(tm) v 2.0
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Shell', 'Console');

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

App::uses('ConnectionManager', 'Model');

App::uses('Controller', 'Controller');
App::uses('ComponentCollection', 'Controller');
App::uses('AclComponent', 'Controller/Component');
App::uses('DbAcl', 'Model');

/**
 * Application Shell
 *
 * Add your application-wide methods in the class below, your shells
 * will inherit them.
 *
 * @package       app.Console.Command
 */
class DeployShell extends Shell 
{    
    public $uses = array(
        'User',
        'Group',
        'Acl'
    );
    
    public $firstgroup = array(
	'Group' => array(
	    'name' => 'Administrators'
	)
    );
	    
    public $firstuser = array(
	'User' => array(
	    'username' => 'admin',
	    'password' => 'admin',
	    'description' => 'first user, login with admin/admin',
	    'group_id' => 1
	)
    );
      
    public function main() 
    {
$this->out('
    Welcome to cake deploy
    
    You have the following functions at your disposal:
    
    - createstructure
	creates a databasestructure based on schema.php

    - creategroup
	creates the first group and the related aro node

    - createuser
	creates the first user and the related aro node

    - updateaco
	check the application for any new actions and syncs them into the aco
	table
	
    - giverights
	gives the first group full application rights (allow all)

    - install
	runs all of the above functions to deploy a new cake app
');
    }
    
    function install()
    {
        //first check if there is a database config
        $dir = new Folder(APP . 'Config');
        $file = $dir->find('database.php');
    
        if(empty($file))
        {
            $this->error('No database configuration found!' ,'Could not find a default database connection. Please create one using cake bake and then rerun the deployer');
        }
        
	$this->createstructure();
	$this->creategroup();
	$this->createuser();
	$this->updateaco();
	$this->giverights();
	$this->updatedbconfig();
	
	$this->out('All operations complete');
    }
        
    function createstructure()
    {
	$dir = new Folder(ROOT . DS . APP_DIR . DS . 'Config' . DS . 'Schema');

        $files = $dir->find('schema.php');

        if(empty($files))
        {
            $this->error('schema.php not found. ', 'Please make sure that the file exists at:' . ROOT . DS . APP_DIR . DS . 'Config' . DS . 'Schema' . DS . 'schema.php');
        } 

        //generate database
	
	$this->dispatchShell('schema create');	        
	
    }
        
    function creategroup()
    {
        //create the first group      
        
        if($this->Group->save($this->firstgroup))
        {
            $this->out('Group ' . $this->firstgroup['Group']['name'] . ' created successfully'); 
        }
        else
        {
            $this->error('Could not create the first group', 'Please make sure that you have a group model');
        }
    }
    
    function createuser()
    {
        if($this->User->save($this->firstuser))
        {
            $this->out('User ' . $this->firstuser['User']['username'] . ' created successfully'); 
        }
        else
        {
            $this->error('Could not create the first user', 'Please make sure that you have a user model');
        }
    }
        
    function updateaco()
    {
	$this->dispatchShell('AclExtras.AclExtras aco_sync');	        
    }
    
    function giverights()
    {
        //give the first user from the first group full application rights
                
        $collection = new ComponentCollection();
        $this->Acl = new AclComponent($collection);
        
        $this->User->Group->id = 1;
        
        if($this->Acl->allow($this->User->Group, 'controllers'))
        {
            $this->out('Gave the ' . $this->firstuser['User']['username'] . ' from group ' . $this->firstgroup['Group']['name'] . ' full permissions'); 
        }
        else
        {
            $this->error('Could not hand out aro rights', 'Please make sure that you have a a valid aco/aro/acos_aros stack');
        }
    }
    
    function updatedbconfig()
    {
        
    }

}

?>